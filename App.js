import React from 'react';
import Login from './src/components/Login'
import ConfigureStore from './src/reducers/ConfigureStore'
import cartReducer from './src/reducers/cartReducer'
import StackRouter from './src/Routes/Router'
import {createStore} from 'redux'
import { Provider } from 'react-redux'

const store = createStore(cartReducer);
function App() {
  return(
  <Provider store= {store}>
    <StackRouter/>
  </Provider>
);
}


export default App;
