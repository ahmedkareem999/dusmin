This is a demo DusMinute Application.

To install the application, clone the repository.

After successfully cloning the repository, use the command "npm install" to install all the dependancies.

Upon successfull installation of packages, run the project using react-native-cli.
You can use "react-native start", "react-native run-android" commands orelse "npx react-native start", "npx react-native run-android".

You can find the android apk build release in the repository and the path exists at "/android/app/build/outputs/apk/release/app-release.apk".

I'm also attaching the apk file in AWS for the easier access and to download and check the App. 
"https://my-vegetables.s3.ap-south-1.amazonaws.com/app-release.apk".