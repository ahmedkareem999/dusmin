import * as React from 'react';
import { Text, View } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createStackNavigator } from '@react-navigation/stack';
import {createSwitchNavigator} from '@react-navigation/compat';
import Icon from 'react-native-vector-icons/FontAwesome';
import Home from '../components/Home'
import Cart from '../components/Cart'
import Logout from '../components/Logout'
import Orders from '../components/Orders'
import Login from '../components/Login'
import OTP from '../components/OTP'

const Tab = createBottomTabNavigator();
const Stack = createStackNavigator();


function tabRouter() {
  return(
    <Tab.Navigator
      screenOptions={({ route }) => ({
          tabBarIcon: ({ focused, color, size }) => {
            let iconName;

            if (route.name === 'Home') {
              iconName = focused
                ? 'home'
                : 'home';
            } else if (route.name === 'Logout') {
              iconName = focused ? 'sign-out' : 'sign-out';
            } else if (route.name === 'Orders') {
              iconName = focused ? 'shopping-bag' : 'shopping-bag'
            } else if (route.name === 'Cart') {
              iconName = focused ? 'shopping-cart' : 'shopping-cart'
            }

            return <Icon name={iconName} size={22} color={color} />;
          },
        })}
        tabBarOptions={{
          activeTintColor: 'tomato',
          inactiveTintColor: 'gray',
        }}>
      <Tab.Screen name="Home" component={Home} />
      <Tab.Screen name="Logout" component={Logout}/>
      <Tab.Screen name = "Orders" component={Orders}/>
      <Tab.Screen name = "Cart" component={Cart}/>

    </Tab.Navigator>
  )
}

export default function StackRouter() {
  return (
    <NavigationContainer>
    <Stack.Navigator screenOptions={{headerShown : false}}>
      <Stack.Screen name="Login" component={Login}/>
      <Stack.Screen name="OTP" component={OTP}/>
      <Stack.Screen name="Home" component={Home}/>
      <Stack.Screen name="Logout" component={Logout}/>
      <Stack.Screen name = "Orders" component={Orders}/>
      <Stack.Screen name = "Cart" component={Cart}/>

    </Stack.Navigator>
    </NavigationContainer>
  );
}
