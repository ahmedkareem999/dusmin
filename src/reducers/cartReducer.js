import { ADD_TO_CART,REMOVE_ITEM,SUB_QUANTITY,
  ADD_QUANTITY,ADD_SHIPPING } from '../actions/cart/index'

import {useSelector} from 'react-redux'

const initState = {
  items : [],
  addedItems :[],
  total : 0,
  quantity : 0
}
const cartReducer= (state = initState,action)=>{

    //INSIDE HOME COMPONENT
    if(action.type === ADD_TO_CART){
            let addedItem = state.items.find(item=> item.id === action.id)
            //check if the action id exists in the addedItems
           let existed_item= state.addedItems.find(item=> action.id === item.id)
           if(existed_item)
           {
              state.quantity += 1
               return{
                  ...state,
                   total: state.total + addedItem.price,
                   quantity : state.quantity
                    }
          }
           else{
              state.quantity = 1;
              //calculating the total
              let newTotal = state.total + addedItem.price

              return{
                  ...state,
                  addedItems: [...state.addedItems, addedItem],
                  total : newTotal,
                  quantity : state.quantity
              }

          }
      }

    if(action.type === REMOVE_ITEM){
        let itemToRemove= state.addedItems.find(item=> action.id === Number(item.id))
        let new_items = state.addedItems.filter(item=> action.id !== Number(item.id))

        //calculating the total
        let newTotal = state.total - (itemToRemove.Price * itemToRemove.quantity )
        console.log(itemToRemove)
        return{
            ...state,
            addedItems: new_items,
            total: newTotal,
            quantity : state.quantity
        }
    }
    //INSIDE CART COMPONENT
    if(action.type=== ADD_QUANTITY){
        let addedItem = state.items.find(item=> item.id === Number(action.id))
          addedItem.quantity += 1
          let newTotal = state.total + addedItem.Price
          return{
              ...state,
              total: newTotal,
              quantity :addedItem.quantity
          }
    }
    if(action.type=== SUB_QUANTITY){
        let addedItem = state.items.find(item=> item.id === Number(action.id))
        //if the qt == 0 then it should be removed
        if(addedItem.quantity === 1){
            let new_items = state.addedItems.filter(item=>item.id !== Number(action.id))
            let newTotal = state.total - addedItem.Price
            return{
                ...state,
                addedItems: new_items,
                total: newTotal
            }
        }
        else {
            addedItem.quantity -= 1
            let newTotal = state.total - addedItem.Price
            return{
                ...state,
                total: newTotal
            }
        }

    }

  else{
    return state
    }

}

export default cartReducer;
