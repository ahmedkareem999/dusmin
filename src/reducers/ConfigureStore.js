import {createStore} from 'redux';
import { cartReducer, initialState } from './cartReducer'

export const ConfigureStore = () => {
    const store = createStore(
        cartReducer, // reducer
        initialState, // our initialState
    );

    return store;
}
