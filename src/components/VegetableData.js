import React, { Component } from 'react';
import {StyleSheet, Image, FlatList,View, ActivityIndicator, TouchableOpacity} from 'react-native';
import { Container, Header, Content, Card, CardItem,
  Thumbnail, Text, Button, Left, Body, Icon, Right } from 'native-base';
import { connect } from 'react-redux'
import { addToCart, subtractQuantity } from '../actions/cart/cartActions'
import AsyncStorage from '@react-native-community/async-storage';

class VegetableData extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dataSource : [],
      isLoading: true,
      isHidden: false
      }

      this.onPress = this.onPress.bind(this);
    }

    onPress() {
      this.setState({isHidden: !this.state.isHidden});
    }

  renderRow = ({item}) => {
    const {
      id,
      name,
      weight,
      Price,
      image
    } = item;
    return (
      <Card style={{flex: 0}}>
          <CardItem>
            <Left>
            <Thumbnail source={{uri: item.image}} style={{height: 100, width:100}}/>
              <Body>
                <Text>{name}</Text>
                <Text note>{weight}</Text>
                <Text>₹{Price}</Text>
              </Body>
            </Left>
            <Right>
            <Add key={id} name={item.name} Price = {item.Price} weight = {item.weight} image = {item.image} />
            </Right>
          </CardItem>
        </Card>
    );
  }

  componentDidMount() {
    const url = "https://my-vegetables.s3.ap-south-1.amazonaws.com/Vegetables.json"

    fetch(url)
    .then((response) => response.json())
    .then((responseJson) => {
      this.setState({
        dataSource : responseJson.items,
        isLoading:false,

      })
    })
    .catch((error) => {
      console.log(error)
    })
  }
  render() {
    return(
      this.state.isLoading
      ?
      <View style = {{flex : 1, justifyContent : "center", alignItems: "center"}}>
       <ActivityIndicator size = "large"/>
       </View>
      :
      <View style ={styles.container}>
        <FlatList
          data = {this.state.dataSource}
          renderItem = {this.renderRow}
          keyExtractor = {(item, index) => index}
          ItemSeparatorComponent = {this.renderSeparator}/>
          </View>
    );
  }
}

class Add extends Component {
  constructor() {
    super ();
  }
  onClickAddCart(item){

   const itemcart = {
     product: item,
     quantity:  1,
     price: this.props.Price,
     name : this.props.name,
     image : this.props.image,
     weight : this.props.weight
   }

   AsyncStorage.getItem('cart').then((datacart)=>{
       if (datacart !== null) {
         const cart = JSON.parse(datacart)
         cart.push(itemcart)
         AsyncStorage.setItem('cart',JSON.stringify(cart));
       }
       else{
         const cart  = []
         cart.push(itemcart)
         AsyncStorage.setItem('cart',JSON.stringify(cart));
       }
       alert("Item added to cart!")
     })
     .catch((err)=>{
       alert(err)
     })
 }
  render() {
    const {id, name, weight, Price, item , image} = this.props;

    return(
      <Button onPress={(item) => {this.onClickAddCart()}} style={{backgroundColor:"#14ada3",
       borderRadius:20,
       width:100,
       textAlign:"center",
       justifyContent:"center"}}>
        <Text>Add</Text>
       </Button>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    justifyContent: 'center'


  }
})

const mapStateToProps = (state)=>{
    return {
      items: state.items,
      addedItems : state.addedItems,
      total: state.total

    }
  }
const mapDispatchToProps= (dispatch)=>{

    return{
        addToCart: (id)=>{dispatch(addToCart(id))},
        subtractQuantity: (id)=>{dispatch(subtractQuantity(id))}
    }
}

export default connect(mapStateToProps,mapDispatchToProps)(VegetableData)
