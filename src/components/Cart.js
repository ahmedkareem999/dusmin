import React, { Component } from 'react';
import {StyleSheet, Image, FlatList,View, ActivityIndicator, TouchableOpacity, ScrollView, RefreshControl} from 'react-native';
import {Container, Card, CardItem, Button, Text, Header, Body, Title, StyleProvider, Content, Left, Right, Thumbnail} from 'native-base'
import Home from './Home'
import VegetableData from './VegetableData'
import CustomFooter from './CustomFooter'
import getTheme from '../../native-base-theme/components';
import material from '../../native-base-theme/variables/material';
import AsyncStorage from '@react-native-community/async-storage';

const wait = (timeout) => {
  return new Promise(resolve => {
    setTimeout(resolve, timeout);
  });
}

export default class Cart extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dataCart:[],

    };
 }

 componentDidMount()
 {
   AsyncStorage.getItem('cart').then((cart)=>{
     if (cart !== null) {
       const cartproducts = JSON.parse(cart)
       this.setState({dataCart:cartproducts})
     } else {
       this.setState({dataCart: []})
     }
   })
   .catch((err)=>{
     alert(err)
   })
 }
  render() {
    const {id, name, weight, Price, count} = this.props;
    return(
      this.state.dataCart && this.state.dataCart.length  > 0 ?
      <StyleProvider style={getTheme(material)}>
        <Container>
        <Header style={{backgroundColor:"#14ada3"}}>
          <Body style={{justifyContent: "center", alignItems: "center", textAlign: "center"}}>
            <Title> CART</Title>
          </Body>
          </Header>
          <Content>
            <ScrollView>
              {
                this.state.dataCart.map((item,id) => {
                  return(
                    <Card style={{flex: 0}}>
                        <CardItem>
                          <Left>
                          <Thumbnail source={{uri: item.image}} style={{height: 100, width:100}}/>
                            <Body>
                              <Text>{item.name}</Text>
                              <Text note>{item.weight}</Text>
                              <Text>₹{item.price}</Text>
                            </Body>
                          </Left>
                          <Right>
                            <Button style={{backgroundColor:"#14ada3", borderRadius:20,width:100 }}>
                              <Text style={{paddingLeft:10, color:"white"}} onPress={()=>{this.onChangeQuantity(id, false)}}>-</Text>
                              <Text style={{color:"white"}}>
                                {item.quantity}
                              </Text>
                                <Text style ={{paddingRight:10, color:"white"}}onPress={()=>{this.onChangeQuantity(id, true)}}>+</Text>
                              </Button>
                              <View style={{paddingTop:10}}>
                                <Text>Total Price: ₹{item.price*item.quantity}</Text>
                              </View>
                          </Right>
                        </CardItem>
                      </Card>
                  );
                })
              }
            </ScrollView>
          </Content>
          <Card>
          <CardItem style={{justifyContent: "center", alignItems: "center"}}>
          <Button onPress= {() => alert("Order successful")} style={{backgroundColor:"#14ada3", borderRadius:20, justifyContent: "center", alignItems: "center"}}>
            <Text> Proceed to Pay </Text>
          </Button>
            <Text style = {{marginLeft:40, fontWeight:"bold", fontSize:20}}>Total Amount: ₹{this.onLoadTotal()}</Text>
          </CardItem>
          </Card>
          <CustomFooter screen="Cart" navigation={this.props.navigation} />
        </Container>
      </StyleProvider>
      :
      <Container>
        <Content>
          <Header style={{backgroundColor:"#14ada3", textAlign: "center", justifyContent: "center"}}>
            <Body>
              <Title> CART</Title>
            </Body>
            </Header>
          <Text>Please add items to the cart!</Text>
        </Content>
        <CustomFooter screen="Home" navigation={this.props.navigation} />
      </Container>
    );
  }
  onLoadTotal() {
   var total = 0
   const cart = this.state.dataCart

   for(var i = 0; i < cart.length; i++) {
     total += cart[i].price*cart[i].quantity
   }
   return total
 }

 async onChangeQuantity(id,type)
 {
   const cart = this.state.dataCart
   let count = cart[id].quantity;

   if (type) {
    count = count + 1
    cart[id].quantity = count
    this.setState({dataCart:cart, cart:cart[id].quantity})
   }
   else if (type==false&&count>=2){
    count = count - 1
    cart[id].quantity = count
    this.setState({dataCart:cart,count:cart[id].quantity})
   }
   else if (type==false&&count==1){
    try {
      cart.splice(id,1)
      await AsyncStorage.setItem("cart",JSON.stringify(cart))
      this.setState({dataCart: JSON.parse(await AsyncStorage.getItem("cart"))})
    } catch(error) {
      alert("Error")
    }


   }
 };
}
