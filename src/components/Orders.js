import React, { Component } from 'react';
import {StyleSheet, Image} from 'react-native';
import { StyleProvider, Container, Header, Content, Footer, FooterTab, Button, Icon, Text, Badge, Left, Body, Right,Title,
  Card,CardItem, Thumbnail} from 'native-base';
import getTheme from '../../native-base-theme/components';
import material from '../../native-base-theme/variables/material';
import CustomFooter from './CustomFooter'


export default class Events extends Component {
  render() {
    return(
      <Container>
      <Header style={{backgroundColor:"#14ada3", textAlign: "center", justifyContent: "center"}}>
        <Body style={{justifyContent: "center", alignItems: "center"}}>
          <Title> ORDERS</Title>
        </Body>
        </Header>
        <Content>
          <Text> No Orders Yet! </Text>
        </Content>
        <CustomFooter screen="Orders" navigation={this.props.navigation} />
      </Container>
    );
  }
}
