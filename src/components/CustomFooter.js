import React, {Component} from 'react';
import {FooterTab, Footer, Button, Text, Badge} from 'native-base';
import { Ionicons } from 'react-native-vector-icons/Ionicons';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { NavigationContainer } from '@react-navigation/native';
import Icon from 'react-native-vector-icons/FontAwesome';
import Home from './Home'
import Logout from './Logout'
import Orders from './Orders'
import Cart from './Cart'

export default class CustomFooter extends Component {
  render() {

      const navigation = this.props.navigation;
      const activeMenu = this.props.screen;
      return (
        <Footer>
          <FooterTab>
            <Button
            onPress={() => navigation.navigate("Home")} >
              <Icon active={activeMenu == 'Home' ? true : false} color={"#ff6347"} name="home" size={22} />
                <Text>Home</Text>

            </Button>

            <Button
               onPress={() => navigation.navigate("Logout")}>
              <Icon active={activeMenu == 'Logout' ? true : false} color={"#ff6347"} name="sign-out" size={22} />
              <Text>Logout</Text>
            </Button>
            <Button
          onPress={() => navigation.navigate("Orders")}  >
              <Icon
                active={activeMenu == 'Orders' ? true : false}
                name="shopping-bag" color={"#ff6347"} size={22}
              />
              <Text>Orders</Text>
            </Button>
            <Button onPress={() => navigation.navigate("Cart")} badge vertical>
              <Icon active={activeMenu == 'Cart' ? true : false} color={"#ff6347"} name="shopping-cart" size={22}/>
              <Text>Cart</Text>
            </Button>
          </FooterTab>
        </Footer>


    );
  }

}
