import React, { Component } from 'react';
import {StyleSheet, Image} from 'react-native';
import { StyleProvider, Container, Header, Content, Footer, FooterTab, Button, Icon, Text, Badge, Left, Body, Right,Title,
  Card,CardItem, Thumbnail} from 'native-base';
//import { Container, Content, Text, StyleProvider } from 'native-base';

import CustomFooter from './CustomFooter';
import VegetableData from './VegetableData';
import getTheme from '../../native-base-theme/components';
import material from '../../native-base-theme/variables/material';
export default class Home extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <StyleProvider style={getTheme(material)}>
        <Container>
        <Header style={{backgroundColor:"#14ada3", textAlign: "center", justifyContent: "center"}}>
          <Body style={{justifyContent: "center", alignItems: "center"}}>
            <Title> FRESH VEGETABLES</Title>
          </Body>
          </Header>
          <Content>
            <VegetableData/>
          </Content>
          <CustomFooter screen="Home" navigation={this.props.navigation} />
        </Container>
      </StyleProvider>
    );
  }
}
