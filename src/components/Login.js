import React, { Component } from 'react';
import {SafeAreaView,View,TextInput, Text, TouchableOpacity, StyleSheet, Image, ScrollView} from  'react-native';
import OTP from './OTP'
export default class Login extends Component {
  constructor(props) {
        super(props);
        this.state = {
            phoneNumber: ''
        }
    }
    renderRandom() {
       const min = 1000;
       const max = 9999;
       const random = (Math.floor(Math.random() * (max - min + 1)) + min);
       return random;
   }

    sendMessage() {
      var phoneNumber = this.state.phoneNumber;
      var api_key = '132fe90f-d97d-11ea-9fa5-0200cd936042';
      var verifyCode = this.renderRandom();
      var url = `https://2factor.in/API/V1/132fe90f-d97d-11ea-9fa5-0200cd936042/SMS/${phoneNumber}/${verifyCode}`
      fetch(url).then(res => {
          if (res.ok) {
            const req = JSON.stringify(res);
            console.log(req);
            const {navigate} = this.props.navigation;
            navigate("OTP", {verifyCode:verifyCode});
          }
          else {
              alert("Error on sending messages. Please try again!");
          }
      }).catch(err => {
          alert("Error on sending message" + err);
      });
    }



  render() {
    return(
      <SafeAreaView style = {styles.container}>
        <View style={styles.subcontainer}>
          <Image source={require('../assets/images/logo_login.jpg')} style = {styles.image}/>
        </View>

      <View style={styles.subcontainer}>
        <TextInput
        placeholder = "Enter your mobile number"
        underlineColorAndroid = "transparent"
        keyboardType = "phone-pad"
        maxLength = {13}
        style = {styles.textInput}
        onChangeText={(text) => this.setState({ phoneNumber: text })}/>

      </View>

        <TouchableOpacity style={styles.button} activeOpacity = { .5 } onPress={() => this.sendMessage()} >
          <Text style = {styles.textbox}>SEND OTP</Text>
        </TouchableOpacity>
      </SafeAreaView>
    );
  }
}


const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',


  },
  subcontainer: {
    justifyContent: 'center',

  },
  image : {

    marginBottom : 20,
    justifyContent : 'center',

  },
  textInput : {
    textAlign : 'center',
    borderWidth : 2,
    borderColor : '#14ada3',
    borderRadius : 20,
    backgroundColor : '#fff',
    justifyContent: 'center',
    margin:10


  },
  button : {
    marginTop: 10,
    padding : 10,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor : '#14ada3',
    borderRadius: 20



  },
  textbox : {
    textAlign: 'center',
    alignItems: 'center',
    justifyContent : 'center',
    color: 'white',
    fontWeight: 'bold',
    fontSize : 20
  }
})
