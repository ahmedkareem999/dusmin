import React, { Component } from 'react';
import {StyleSheet, Image} from 'react-native';
import { StyleProvider, Container, Header, Content, Footer, FooterTab, Button, Icon, Text, Badge, Left, Body, Right,Title,
  Card,CardItem, Thumbnail} from 'native-base';
import getTheme from '../../native-base-theme/components';
import material from '../../native-base-theme/variables/material';
import CustomFooter from './CustomFooter'

export default class Events extends Component {
  render() {
    return(
      <Container>
      <Header style={{backgroundColor:"#14ada3", textAlign: "center", justifyContent: "center"}}>
        <Body>
          <Title>My Profile</Title>
        </Body>
        </Header>
        <Content>
          <Text>To Logout click on logout button</Text>
          <Button  onPress={this.props.navigation.navigate("Login")}>
            <Text>LOGOUT</Text>
          </Button>
        </Content>
        <CustomFooter screen="Events" navigation={this.props.navigation} />
      </Container>
    );
  }
}
