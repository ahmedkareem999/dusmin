import React, { Component } from 'react';
import {SafeAreaView, View, TextInput, Image, StyleSheet, TouchableOpacity, Text} from 'react-native';
import Home from './Home'
export default class OTP extends Component {
  componentDidMount() {
        this._unsubscribe = this.props.navigation.addListener('focus', () => {
            var otp = this.props.route.params.verifyCode;
            if (this.state.verifyCode != otp) {
                this.setState({ verifyCode: otp });
            }
        });
    }

    componentWillUnmount() {
        this._unsubscribe();
    }

    constructor(props) {
        super(props);
        var otp = this.props.route.params.verifyCode;
        this.state = {
            codeActivationFromClient: '',
            verifyCode : otp
        }
    }

  render()  {
    return(
      <SafeAreaView style = {styles.container}>
        <View style={styles.subcontainer}>
          <Image source={require('../assets/images/logo_Otp.jpg')} style = {styles.image}/>
        </View>
        <View style={styles.subcontainer}>
          <TextInput
          placeholder = "Enter your OTP"
          underlineColorAndroid = "transparent"
          keyboardType = "numeric"
          maxLength = {4}
          onChangeText={(text) => this.setState({ codeActivationFromClient: text })}
          style = {styles.textInput}/>

      </View>
        <TouchableOpacity onPress = {() => this.props.navigation.navigate("Login")} style={styles.changeNumber}>
          <Text style={{color:"white"}}> Change Number </Text>
        </TouchableOpacity>

        <TouchableOpacity onPress={() => this.verify()} style={styles.button} activeOpacity = { .5 }>
          <Text style = {styles.textbox}>VERIFY</Text>
        </TouchableOpacity>
      </SafeAreaView>
    );
  }
  verify() {
        if (this.state.verifyCode == this.state.codeActivationFromClient) {
            const { navigate } = this.props.navigation;
            navigate("Home");
        }
        else {
            alert("Verify code is not correct!");
        }
    }
}

const styles = StyleSheet.create({
  container: {
    flex : 1
  },
  subcontainer: {
    justifyContent : 'space-evenly',

  },
  image : {
    marginBottom : 20
  },
  otp : {
    flexDirection : 'row',
    justifyContent : 'space-evenly'

  },
  changeNumber : {
    flex:0.2,
    margin:20,
     alignItems: "center",
     justifyContent:"center",
     backgroundColor:"#14ada3",
     borderRadius:20,
     width:150
  },
  textInput : {
    textAlign : 'center',
    borderWidth : 2,
    borderColor : '#14ada3',
    borderRadius : 20,
    backgroundColor : '#fff',
    justifyContent: 'center',
    margin:10
  },
  button: {
    marginTop: 10,
    padding : 10,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor : '#14ada3',
    borderRadius: 20
  },
  textbox : {
    textAlign: 'center',
    alignItems: 'center',
    justifyContent : 'center',
    color: 'white',
    fontWeight: 'bold',
    fontSize : 20
  }
})
